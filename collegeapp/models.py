from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User


#/////////////////////////////////models for hod///////////////////////////////////////////////////

class staffreg(models.Model):
    user = models.OneToOneField('auth.user', on_delete=models.CASCADE)
    staff_id = models.IntegerField(null = True)
    staff_name = models.CharField(max_length = 200)
    department= models.CharField(max_length=50, null=True)
    designation= models.CharField(max_length=50, null=True)
    mobile = models.IntegerField(null = True)
    saved_time = models.DateTimeField(default=timezone.now)

class studentreg(models.Model):
    user = models.OneToOneField('auth.user', on_delete=models.CASCADE)
    student_id = models.IntegerField(null = True)
    student_name = models.CharField(max_length = 200)
    department= models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=10, null=True)
    birth_date = models.DateField(null = True)
    mobile = models.IntegerField(null = True)
    saved_time = models.DateTimeField(default=timezone.now)


class syllabus(models.Model):
    department_name = models.CharField(max_length = 200)
    syllabus_Upload = models.ImageField(upload_to='profile',default="")
    subject_hours= models.IntegerField(null = True)

class events(models.Model):
    event_name =  models.CharField(max_length = 200)
    event_startdate = models.DateField(null = True)
    event_enddate = models.DateField(null = True)
    location =  models.CharField(max_length = 200)
    Staff_Incharge =  models.CharField(max_length = 200)




class staffleav(models.Model):
    staff_id = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    staff_name = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(staffreg, on_delete=models.CASCADE)
    leave_status = models.CharField(max_length = 200)

class studleav(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(staffreg, on_delete=models.CASCADE)
    leave_status = models.CharField(max_length = 200)





#/////////////////////////////////models for student///////////////////////////////////////////////////

















#/////////////////////////////////models for staff///////////////////////////////////////////////////
    

class attendance(models.Model):
    
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    date = models.DateField()
    is_present = models.BooleanField(default=False)


class addgrade(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    grade =  models.CharField(max_length = 200)
   

class applyleave(models.Model):
    staff_id = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    staff_name = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(staffreg, on_delete=models.CASCADE)
    reason = models.TextField(max_length = 200)
    start_date = models.DateField()
    end_date = models.DateField()

class staff_feedback(models.Model):
    staff_id = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    staff_name = models.ForeignKey(staffreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(staffreg, on_delete=models.CASCADE)
    feedback = models.TextField(max_length = 200)
    



#/////////////////////////////////models for student///////////////////////////////////////////////////
    


class staff_feedback(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(staffreg, on_delete=models.CASCADE)
    feedback = models.TextField(max_length = 200)


class stud_applyleave(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(studentreg, on_delete=models.CASCADE)
    reason = models.TextField(max_length = 200)
    start_date = models.DateField()
    end_date = models.DateField()

class contactplacement(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(studentreg, on_delete=models.CASCADE)
    message = models.TextField(max_length = 200)






#/////////////////////////////////models for placementcell////////////////////////////////////////////////
    
class companies(models.Model):
    company_name = models.CharField(max_length = 200)
    salary =  models.IntegerField(null = True)
    location = models.CharField(max_length = 200)
   
class allocatestudent(models.Model):
    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(studentreg, on_delete=models.CASCADE)
    company_name = models.ForeignKey(companies, on_delete=models.CASCADE)


class replaying_for_contact(models.Model):

    student_id = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    student_name = models.ForeignKey(studentreg, on_delete=models.CASCADE)
    department =  models.ForeignKey(studentreg, on_delete=models.CASCADE)
    replay_message = models.TextField(max_length = 200)


#/////////////////////////////////models for fee management////////////////////////////////////////////////
    


class fee_allocation(models.Model):
    department_name = models.CharField(max_length = 200)
    amount_per_semester = models.IntegerField(null = True) 