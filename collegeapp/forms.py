
from django import forms

from .models import staffreg

from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm
from django import forms


from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm



class CreateUserForm(UserCreationForm):
    staff_id = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    fullname = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
   
    department = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    designation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    mobile = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username','email','password1','password2','fullname','department','designation','mobile')
    
class CreateUserFormNew(UserCreationForm):
    GENDER_CHOICES = [('Male', 'Male'), ('Female', 'Female')]
    DEPT_CHOICES = [('Zoology', 'Zoology'), ('Botany', 'Botany'), ('Bca', 'Bca')]
    student_id = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    fullname = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
   
    department = forms.ChoiceField(choices=DEPT_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    designation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.RadioSelect(attrs={'class': 'form-check-input'}))
    birth_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))

    mobile = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username','email','password1','password2','student-id','fullname','department','designation','gender','birth_date','mobile')